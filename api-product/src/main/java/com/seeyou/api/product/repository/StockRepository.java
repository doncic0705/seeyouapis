package com.seeyou.api.product.repository;


import com.seeyou.api.product.entity.Product;
import com.seeyou.api.product.entity.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, Long> {
    Page<Stock> findAllByIdGreaterThanEqualOrderByStockQuantity (long id, Pageable pageable);
    Stock findByProduct (Product product);
}
