package com.seeyou.api.product.repository;

import com.seeyou.api.product.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Page<Product> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
}
