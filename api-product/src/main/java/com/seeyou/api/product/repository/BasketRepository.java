package com.seeyou.api.product.repository;


import com.seeyou.api.product.entity.Basket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BasketRepository extends JpaRepository<Basket, Long> {
}
