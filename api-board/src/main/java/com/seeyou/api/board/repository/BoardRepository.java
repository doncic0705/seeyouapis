package com.seeyou.api.board.repository;

import com.seeyou.api.board.entity.Board;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<Board, Long> {
    Page<Board> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
}
