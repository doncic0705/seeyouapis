package com.seeyou.api.income.repository;

import com.seeyou.api.income.entity.Income;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IncomeRepository extends JpaRepository<Income, Long> {
    Page<Income> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
    List<Income> findAllByIncomeYearAndIncomeMonth(String year, String month);
}
