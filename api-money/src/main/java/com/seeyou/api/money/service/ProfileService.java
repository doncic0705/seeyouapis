package com.seeyou.api.money.service;

import com.seeyou.api.money.entity.Member;
import com.seeyou.api.money.repository.MemberRepository;
import com.seeyou.common.exception.CAccessDeniedException;
import com.seeyou.common.exception.CMissingDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final MemberRepository memberRepository;

    public Member getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Member member = memberRepository.findByUsername(username).orElseThrow(CMissingDataException::new);
        if (!member.getIsEnabled()) throw new CAccessDeniedException();
        return member;
    }
}
