package com.seeyou.api.money.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneyHistorySearchRequest {
    private String moneyYear;
    private String moneyMonth;
    private String name;
}
