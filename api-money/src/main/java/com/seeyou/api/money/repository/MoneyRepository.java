package com.seeyou.api.money.repository;

import com.seeyou.api.money.entity.Money;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoneyRepository extends JpaRepository<Money, Long> {
    Page<Money> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
}
