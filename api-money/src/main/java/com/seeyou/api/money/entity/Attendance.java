package com.seeyou.api.money.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private Integer attendanceYear;

    @Column(nullable = false)
    private Integer attendanceMonth;

    @Column(nullable = false)
    private Integer attendanceDay;

    @Column(nullable = false)
    private LocalTime timeAttendance;

    private LocalTime timeLeaveWork;
}