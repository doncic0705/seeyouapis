package com.seeyou.api.money.controller;

import com.seeyou.api.money.entity.Member;
import com.seeyou.api.money.model.MoneyHistoryItem;
import com.seeyou.api.money.model.MoneyHistoryRequest;
import com.seeyou.api.money.model.MoneyHistoryResponse;
import com.seeyou.api.money.model.MoneyHistorySearchRequest;
import com.seeyou.api.money.service.MemberDataService;
import com.seeyou.api.money.service.MoneyHistoryService;
import com.seeyou.api.money.service.ProfileService;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.model.SingleResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "급여 지급 내역")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/money-history")
public class MoneyHistoryController {
    private final MoneyHistoryService moneyHistoryService;
    private final MemberDataService memberDataService;
    private final ProfileService profileService;

    @ApiOperation(value = "급여 지급 내역 등록")
    @PostMapping("/member-id/{memberId}")
    public CommonResult setMoneyHistory(@PathVariable long memberId, @RequestBody @Valid MoneyHistoryRequest moneyHistoryRequest) {
        Member member = memberDataService.getMemberData(memberId);
        moneyHistoryService.setMoneyHistory(member, moneyHistoryRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "급여 지급 내역 리스트")
    @GetMapping("/all/{page}")
    public ListResult<MoneyHistoryItem> getList(
            @PathVariable int page,
            @RequestParam(value = "searchYear", required = false) String searchYear,
            @RequestParam(value = "searchMonth", required = false) String searchMonth,
            @RequestParam(value = "searchName", required = false) String searchName
            ) {
        MoneyHistorySearchRequest request = new MoneyHistorySearchRequest();
        request.setMoneyYear(searchYear);
        request.setMoneyMonth(searchMonth);
        request.setName(searchName);
        return ResponseService.getListResult(moneyHistoryService.getList(page, request), true);
    }

    @ApiOperation(value = "급여 지급 내역 리스트 상세")
    @GetMapping("/history")
    public SingleResult<MoneyHistoryResponse> getMoneyHistory(@RequestParam("id") long id) {
        return ResponseService.getSingleResult(moneyHistoryService.getMoneyHistory(id));
    }

    @ApiOperation(value = "나의 급여 지급 내역 리스트")
    @GetMapping("/my-histories")
    public ListResult<MoneyHistoryItem> getMyMoneyHistories() {
        Member member = profileService.getMemberData();
        return ResponseService.getListResult(moneyHistoryService.getMyMoneyHistories(member), true);
    }

    @ApiOperation(value = "나의 급여 지급 내역 리스트")
    @GetMapping("/my-history")
    public SingleResult<MoneyHistoryResponse> getMyMoneyHistory(@RequestParam("id") long id) {
        Member member = profileService.getMemberData();
        return ResponseService.getSingleResult(moneyHistoryService.getMyMoneyHistory(id, member));
    }
}