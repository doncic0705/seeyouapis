package com.seeyou.api.money.model;

import com.seeyou.api.money.entity.Money;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyResponse {
    @ApiModelProperty(notes = "직원명")
    private String member;

    @ApiModelProperty(notes = "급여타입")
    private String moneyType;

    @ApiModelProperty(notes = "세전금액")
    private BigDecimal beforeMoney;

    @ApiModelProperty(notes = "국민연금")
    private BigDecimal nationalPension;

    @ApiModelProperty(notes = "건강보험")
    private BigDecimal healthInsurance;

    @ApiModelProperty(notes = "장기요양보험")
    private BigDecimal longTermCareInsurance;

    @ApiModelProperty(notes = "고용보험")
    private BigDecimal employmentInsurance;

    @ApiModelProperty(notes = "산재보험")
    private BigDecimal industrialAccidentInsurance;

    @ApiModelProperty(notes = "소득세")
    private BigDecimal incomeTax;

    @ApiModelProperty(notes = "지방소득세")
    private BigDecimal localIncomeTax;

    private MoneyResponse(Builder builder) {
        this.member = builder.member;
        this.moneyType = builder.moneyType;
        this.beforeMoney = builder.beforeMoney;
        this.nationalPension = builder.nationalPension;
        this.healthInsurance = builder.healthInsurance;
        this.longTermCareInsurance = builder.longTermCareInsurance;
        this.employmentInsurance = builder.employmentInsurance;
        this.industrialAccidentInsurance = builder.industrialAccidentInsurance;
        this.incomeTax = builder.incomeTax;
        this.localIncomeTax = builder.localIncomeTax;
    }

    public static class Builder implements CommonModelBuilder<MoneyResponse> {
        private final String member;
        private final String moneyType;
        private final BigDecimal beforeMoney;
        private final BigDecimal nationalPension;
        private final BigDecimal healthInsurance;
        private final BigDecimal longTermCareInsurance;
        private final BigDecimal employmentInsurance;
        private final BigDecimal industrialAccidentInsurance;
        private final BigDecimal incomeTax;
        private final BigDecimal localIncomeTax;

        public Builder(Money money) {
            this.member = money.getMember().getMemberName();
            this.moneyType = money.getMoneyType().getName();
            this.beforeMoney = money.getBeforeMoney();
            this.nationalPension = money.getNationalPension();
            this.healthInsurance = money.getHealthInsurance();
            this.longTermCareInsurance = money.getLongTermCareInsurance();
            this.employmentInsurance = money.getEmploymentInsurance();
            this.industrialAccidentInsurance = money.getIndustrialAccidentInsurance();
            this.incomeTax = money.getIncomeTax();
            this.localIncomeTax = money.getLocalIncomeTax();
        }

        @Override
        public MoneyResponse build() {
            return new MoneyResponse(this);
        }
    }
}