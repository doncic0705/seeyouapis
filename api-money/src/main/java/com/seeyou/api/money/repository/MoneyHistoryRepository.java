package com.seeyou.api.money.repository;

import com.seeyou.api.money.entity.MoneyHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoneyHistoryRepository extends JpaRepository<MoneyHistory, Long> {
}
