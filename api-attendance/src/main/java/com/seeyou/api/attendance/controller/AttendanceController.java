package com.seeyou.api.attendance.controller;

import com.seeyou.api.attendance.entity.Member;
import com.seeyou.api.attendance.model.AttendanceItem;
import com.seeyou.api.attendance.model.AttendanceSearchRequest;
import com.seeyou.api.attendance.service.AttendanceService;
import com.seeyou.api.attendance.service.ProfileService;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RequestMapping("/v1/attendance")
@RestController
@Api(tags = "근태 관리")
public class AttendanceController {
    private final AttendanceService attendanceService;
    private final ProfileService profileService;

    @PostMapping("/new")
    @ApiOperation("근태 등록")
    public CommonResult setAttendance() {
        Member member = profileService.getMemberData();
        attendanceService.setAttendance(member);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("/leave-work")
    @ApiOperation(value = "퇴근하기")
    public CommonResult putLeaveWork() {
        Member member = profileService.getMemberData();
        attendanceService.putLeaveWork(member);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all/{page}")
    @ApiOperation(value = "근태 리스트")
    public ListResult<AttendanceItem> getList(
            @PathVariable int page,
            @RequestParam(value = "searchYear", required = false) String searchYear,
            @RequestParam(value = "searchMonth", required = false) String searchMonth,
            @RequestParam(value = "searchDay", required = false) String searchDay,
            @RequestParam(value = "searchName", required = false) String searchName
            ) {
        AttendanceSearchRequest request = new AttendanceSearchRequest();
        request.setAttendanceYear(searchYear);
        request.setAttendanceMonth(searchMonth);
        request.setAttendanceDay(searchDay);
        request.setName(searchName);
        return ResponseService.getListResult(attendanceService.getList(page, request), true);
    }

    @GetMapping("/my")
    @ApiOperation(value = "나의 근태 리스트")
    public ListResult<AttendanceItem> getMyList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        Member member = profileService.getMemberData();
        return ResponseService.getListResult(attendanceService.getMyList(page, member), true);
    }
}