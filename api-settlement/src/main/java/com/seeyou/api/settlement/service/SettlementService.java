package com.seeyou.api.settlement.service;

import com.seeyou.api.settlement.entity.Settlement;
import com.seeyou.api.settlement.model.SettlementItem;
import com.seeyou.api.settlement.model.SettlementResponse;
import com.seeyou.api.settlement.repository.ProductOrderRepository;
import com.seeyou.api.settlement.repository.SettlementRepository;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SettlementService {
    private final SettlementRepository settlementRepository;

    public ListResult<SettlementItem> getList(int page) {
        Page<Settlement> originList = settlementRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<SettlementItem> result = new LinkedList<>();
        for (Settlement settlement : originList) {
            result.add(new SettlementItem.Builder(settlement).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public SettlementResponse getSettlement(int year, String month) {
        List<Settlement> originList = settlementRepository.findAllBySettlementYearAndSettlementMonth(year, month);
        BigDecimal price = BigDecimal.valueOf(0);
        for (Settlement settlement : originList) {
            price = price.add(settlement.getPrice());
        }
        return new SettlementResponse.Builder(price).build();
    }

    public void putSettlement(int year, String month) {
        List<Settlement> originList = settlementRepository.findAllBySettlementYearAndSettlementMonth(year, month);
        for (Settlement settlement : originList) {
            settlement.putIsComplete();
            settlementRepository.save(settlement);
        }
    }
}