package com.seeyou.api.settlement.repository;

import com.seeyou.api.settlement.entity.ProductOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductOrderRepository extends JpaRepository<ProductOrder, Long> {
}
