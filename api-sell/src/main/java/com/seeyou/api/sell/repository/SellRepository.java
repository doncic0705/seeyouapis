package com.seeyou.api.sell.repository;

import com.seeyou.api.sell.entity.Sell;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SellRepository extends JpaRepository<Sell, Long> {
    Page<Sell> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
    List<Sell> findAllByBillNumber (long billNumber);
}
