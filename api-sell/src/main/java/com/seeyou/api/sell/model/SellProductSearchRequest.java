package com.seeyou.api.sell.model;

import com.seeyou.common.enums.SellProductType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SellProductSearchRequest {
    private SellProductType sellProductType;
    private String productName;
}
