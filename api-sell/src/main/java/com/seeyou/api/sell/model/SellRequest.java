package com.seeyou.api.sell.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SellRequest {
    @ApiModelProperty(notes = "영수증 번호", required = true)
    @NotNull
    private Long billNumber;

    @ApiModelProperty(notes = "수량", required = true)
    @NotNull
    @Min(value = 0)
    private Integer quantity;

    @ApiModelProperty(notes = "금액", required = true)
    @NotNull
    @Min(value = 0)
    private Double price;
}
